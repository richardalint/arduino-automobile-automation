// Arduino Automobile Automation  //
//        By Richard Lint         //

/*
 * PIN 2 to LOCK/A1
 * PIN 9 to RST/A16
 * PIN 10 to SDA/A10
 * PIN 11 to MOSI/A12
 * PIN 12 to MISO/A13
 * PIN 13 to SCK/A11
 * GND to GND on RFID (A15)/GND rail
 * 3.3V to 3.3V/A17
 */
#include "SPI.h"
#include "MFRC522.h"
#include "myKey.h"

#define SS_PIN 10
#define RST_PIN 9
#define SP_PIN 8

#define LOCK_PIN 2
#define UNLOCK_PIN 3

MFRC522 rfid(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key;

void setup() {
  Serial.begin(9600);
  // Set the TIP pin to be an output
  pinMode(LOCK_PIN, OUTPUT);
  pinMode(UNLOCK_PIN, OUTPUT);
  SPI.begin();
  rfid.PCD_Init();
}

void loop() { 
  if (!rfid.PICC_IsNewCardPresent() || !rfid.PICC_ReadCardSerial())
    return;

  // Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  // Check if the PICC is Classic MIFARE
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_UL) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }

  // Reset the string of the card key to be blank
  String strID = "";
  // Read the bytes from the card key's UID
  for (byte i = 0; i < 4; i++) {
    strID +=
      // Format card key string as you parse
      (rfid.uid.uidByte[i] < 0x10 ? "0" : "") +
      String(rfid.uid.uidByte[i], HEX) +
      (i != 3 ? ":" : "");
  }
  // Convert they key to upper case
  strID.toUpperCase();

  // Check if the card scanned is registered to unlock the door
  if (strID == CARD_KEY){
    // If it is registered, send the signal to the lock TIP 220
    Serial.println("Unlocking");
    digitalWrite(LOCK_PIN, HIGH);
    delay(1000);
    digitalWrite(LOCK_PIN, LOW);
    delay(5000);
    // If it is registered, send the signal to the unlock TIP 220
    Serial.println("Locking");
    digitalWrite(UNLOCK_PIN, HIGH);
    delay(1000);
    digitalWrite(UNLOCK_PIN, LOW);
  }

  // Print out the card key that was tapped
  Serial.print("Tapped card key: ");
  Serial.println(strID);

  rfid.PICC_HaltA();
  rfid.PCD_StopCrypto1();
}
